package cl.duoc.persistenciadedatos;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

/**
 * Created by Duoc on 18-05-2016.
 */
public class SesionPreferences {

    private static final String NOMBRE_SHARED = "sesion_usuario";
    private static final String KEY_NOMBRE = "key_nombre";
    private static final String KEY_NOMBRE_CLIENTE = "key_nombre_cliente";
    private static final String KEY_EDAD_CLIENTE = "key_edad_cliente";
    private static final String KEY_RUT_CLIENTE = "key_rut_cliente";

    public static final String KEY_USUARIO_JSON = "key_usuario_json";

    private static SesionPreferences instance;
    
    private SesionPreferences(){}
    
    public static SesionPreferences getInstance(){
        
        if(instance == null){
            instance = new SesionPreferences();            
        }
        return instance;
    }

    public void guardarSesion(Context context, String nombre) {
        SharedPreferences sha = context.getSharedPreferences(NOMBRE_SHARED, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sha.edit();
        edit.putString(KEY_NOMBRE, nombre);
        edit.commit();
    }

    public void cerrarSesion(Context context) {
        SharedPreferences sha = context.getSharedPreferences(NOMBRE_SHARED, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sha.edit();
        edit.putString(KEY_NOMBRE, "");
        edit.commit();
    }

    public String obtenerSesion(Context context) {
        SharedPreferences sha = context.getSharedPreferences(NOMBRE_SHARED, Context.MODE_PRIVATE);
        String nombre = sha.getString(KEY_NOMBRE, "");
        return nombre;
    }


    public void guardarCliente(Context context, String nombreCliente, String rutCliente, int edadCliente) {
        SharedPreferences sha = context.getSharedPreferences(NOMBRE_SHARED, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sha.edit();
        edit.putString(KEY_NOMBRE_CLIENTE, nombreCliente);
        edit.putString(KEY_RUT_CLIENTE, rutCliente);
        edit.putInt(KEY_EDAD_CLIENTE, edadCliente);
        edit.commit();
    }

    public Cliente obtenerCliente(Context context) {
        Cliente cliente = new Cliente();
        SharedPreferences sha = context.getSharedPreferences(NOMBRE_SHARED, Context.MODE_PRIVATE);
        String nombreCliente = sha.getString(KEY_NOMBRE_CLIENTE, "");
        String rutCliente = sha.getString(KEY_RUT_CLIENTE, "");
        int edadCliente = sha.getInt(KEY_EDAD_CLIENTE, 0);
        cliente.setEdadCliente(edadCliente);
        cliente.setNombreCliente(nombreCliente);
        cliente.setRutCliente(rutCliente);
        return cliente;
    }


    public void guardarUsuario(Context context, String usuarioJson) {
        SharedPreferences sha = context.getSharedPreferences(NOMBRE_SHARED, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sha.edit();
        edit.putString(KEY_USUARIO_JSON, usuarioJson);
        edit.commit();
    }

    public Usuario obtenerUsuario(Context context) {
        Usuario usuario = null;
        SharedPreferences sha = context.getSharedPreferences(NOMBRE_SHARED, Context.MODE_PRIVATE);
        try {
            String usuarioJson = sha.getString(KEY_USUARIO_JSON, "");
            Gson gson = new Gson();
            usuario = gson.fromJson(usuarioJson, Usuario.class);
        }catch (Exception e){
        }

        return usuario;
    }
    
}












