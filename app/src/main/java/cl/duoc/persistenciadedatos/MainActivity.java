package cl.duoc.persistenciadedatos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.TooManyListenersException;

public class MainActivity extends AppCompatActivity {

    private Button btnValidar;
    private Button btnCrearCliente;
    private Button btnCrearUsuario;
    private EditText txtNombre;
    private EditText txtClave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnValidar = (Button)findViewById(R.id.btnValidar);
        btnValidar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validarUsuario();
            }
        });

        btnCrearUsuario= (Button)findViewById(R.id.btnCrearUsuario);
        btnCrearUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crearUsuario();
            }
        });


        btnCrearCliente = (Button)findViewById(R.id.btnCrearCliente);
        btnCrearCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crearCliente();
            }
        });

        txtNombre = (EditText)findViewById(R.id.txtNombre);
        txtClave = (EditText)findViewById(R.id.txtClave);

        if(SesionPreferences.getInstance().obtenerSesion(this).length()>0){
            cambiarVentana();
        }
    }

    private void crearUsuario() {
        Intent i = new Intent(this, UsuarioActivity.class);
        startActivity(i);
    }

    private void crearCliente() {
        Intent i = new Intent(this, CrearClienteActivity.class);
        startActivity(i);
    }

    private void validarUsuario() {
        if(txtNombre.getText().toString().equals("admin") &&
                txtClave.getText().toString().equals("1234")) {
            SesionPreferences.getInstance().guardarSesion(this, txtNombre.getText().toString());
            cambiarVentana();
        }
        else {
            Toast.makeText(this, "Usuario invalido", Toast.LENGTH_LONG).show();
        }
    }

    private void cambiarVentana() {
        Intent i = new Intent(this, MenuPrincipalActivity.class);
        startActivity(i);
        finish();
    }


}
