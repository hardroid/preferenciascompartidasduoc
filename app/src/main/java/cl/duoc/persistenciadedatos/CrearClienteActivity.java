package cl.duoc.persistenciadedatos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CrearClienteActivity extends AppCompatActivity {

    private Button btnGuardarCliente;
    private EditText txtEdad;
    private EditText txtNombre;
    private EditText txtRut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_cliente);

        txtEdad = (EditText)findViewById(R.id.txtEdad);
        txtRut = (EditText)findViewById(R.id.txtRut);
        txtNombre = (EditText)findViewById(R.id.txtNombre);

        btnGuardarCliente = (Button)findViewById(R.id.btnGuardarCliente);
        btnGuardarCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guadarCliente();
            }
        });

        cargarDatosSesion();

    }

    private void guadarCliente() {
        SesionPreferences.getInstance().guardarCliente(
                this,
                txtNombre.getText().toString(),
                txtRut.getText().toString(),
                Integer.parseInt(txtEdad.getText().toString()));
        Toast.makeText(this, "Cliente Guardado", Toast.LENGTH_LONG).show();
    }


    private void cargarDatosSesion(){
       Cliente c = SesionPreferences.getInstance().obtenerCliente(this);
        txtNombre.setText(c.getNombreCliente());
        txtRut.setText(c.getRutCliente());
        txtEdad.setText(c.getEdadCliente()+"");
    }

}
