package cl.duoc.persistenciadedatos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;

public class UsuarioActivity extends AppCompatActivity {

    private Button btnGuardar;
    private EditText txtId, txtCodUsuario, txtNombreUsuario, txtCorreo,
            txtTelefono, txtFechaNacimiento, txtContrasena, txtDireccion,
            txtComuna, txtTwitter, txtInstagram, txtFacebookLink, txtProfesion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario);

        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                guardarUsuario();
            }
        });

        txtId = (EditText) findViewById(R.id.txtId);
        txtCodUsuario = (EditText) findViewById(R.id.txtCodUsuario);
        txtNombreUsuario = (EditText) findViewById(R.id.txtNombreUsuario);
        txtCorreo = (EditText) findViewById(R.id.txtCorreo);
        txtTelefono = (EditText) findViewById(R.id.txtTelefono);
        txtFechaNacimiento = (EditText) findViewById(R.id.txtFechaNacimiento);
        txtContrasena = (EditText) findViewById(R.id.txtContrasena);
        txtDireccion = (EditText) findViewById(R.id.txtDireccion);
        txtComuna = (EditText) findViewById(R.id.txtComuna);
        txtTwitter = (EditText) findViewById(R.id.txtTwitter);
        txtInstagram = (EditText) findViewById(R.id.txtInstagram);
        txtFacebookLink = (EditText) findViewById(R.id.txtFacebookLink);
        txtProfesion = (EditText) findViewById(R.id.txtProfesion);

        cargarDatos();


    }

    private void cargarDatos() {
        Usuario usuario = SesionPreferences.getInstance().obtenerUsuario(this);
        if(usuario != null){
            txtId.setText(usuario.getId()+"");
            txtCodUsuario.setText(usuario.getCodUsuario());
            txtNombreUsuario.setText(usuario.getNombreUsuario());
            txtCorreo.setText(usuario.getCorreo());
            txtTelefono.setText(usuario.getTelefono());
            txtFechaNacimiento.setText(usuario.getFechaNacimiento());
            txtContrasena.setText(usuario.getContrasena());
            txtDireccion.setText(usuario.getDireccion());
            txtComuna.setText(usuario.getComuna());
            txtTwitter.setText(usuario.getTwitter());
            txtInstagram.setText(usuario.getInstagram());
            txtFacebookLink.setText(usuario.getFacebookLink());
            txtProfesion.setText(usuario.getProfesion());
        }
    }

    private void guardarUsuario() {
        Usuario usuario = new Usuario();
        usuario.setId(Integer.parseInt(txtId.getText().toString()));
        usuario.setCodUsuario(txtCodUsuario.getText().toString());
        usuario.setComuna(txtComuna.getText().toString());
        usuario.setContrasena(txtContrasena.getText().toString());
        usuario.setCorreo(txtCorreo.getText().toString());
        usuario.setDireccion(txtDireccion.getText().toString());
        usuario.setFacebookLink(txtFacebookLink.getText().toString());
        usuario.setFechaNacimiento(txtFechaNacimiento.getText().toString());
        usuario.setTwitter(txtTwitter.getText().toString());
        usuario.setInstagram(txtInstagram.getText().toString());
        usuario.setNombreUsuario(txtNombreUsuario.getText().toString());
        usuario.setProfesion(txtProfesion.getText().toString());
        usuario.setTelefono(txtTelefono.getText().toString());

        Gson gson = new Gson();
        String usuarioJson = gson.toJson(usuario);
        SesionPreferences.getInstance().guardarUsuario(this, usuarioJson);



    }
}
