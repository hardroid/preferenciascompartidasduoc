package cl.duoc.persistenciadedatos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MenuPrincipalActivity extends AppCompatActivity {

    private Button btnCerrarSesion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);
        String nombre = SesionPreferences.getInstance().obtenerSesion(this);
        Toast.makeText(this, nombre, Toast.LENGTH_LONG).show();

        btnCerrarSesion = (Button)findViewById(R.id.btnCerrarSesion);
        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cerrarSesion();
            }
        });
    }

    private void cerrarSesion() {
        SesionPreferences.getInstance().cerrarSesion(this);
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }


}
